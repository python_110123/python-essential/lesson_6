# Реалізуйте цикл, який перебиратиме всі значення ітерабельного
# об'єкту iterable

iterable = [1, 2, 3, 4]

iterator = iter(iterable)
for value in iterator:
    print(value)