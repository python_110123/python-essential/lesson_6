# Напишіть ітератор, який повертає елементи заданого списку
# у зворотному порядку (аналог reversed).


class MyList:

    def __init__(self, iterable=None):
        self.data = iterable[::-1]  # зворотний порядок

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        if self.index >= len(self.data):
            raise StopIteration
        value = self.data[self.index]
        self.index += 1
        return value


my_list = MyList([1, 2, 5])
for number in my_list:
    print(number)
